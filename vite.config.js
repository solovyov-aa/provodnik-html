import { resolve } from 'path'
import { defineConfig } from 'vite'

export default defineConfig({
  base: '/html',
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        home: resolve(__dirname, './pages/home.html'),
        template: resolve(__dirname, './pages/template.html'),
        contacts: resolve(__dirname, './pages/contacts.html'),
        userInfo: resolve(__dirname, './pages/user-info.html'),
        pressCenter: resolve(__dirname, './pages/press-center.html'),
        about: resolve(__dirname, './pages/about.html'),
      }
    }
  }
})