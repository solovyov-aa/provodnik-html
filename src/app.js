import { setViewportHeigth } from './js/viewport-height'
import { pageScrollHandler } from './js/page-scroll-handler'
import { setLayoutShift } from './js/layout-shift';
import { toggleMenu } from './js/toggle-menu';
import { initSwipers } from './js/swipers';
import { initAnimations } from './js/animations';

window.addEventListener('DOMContentLoaded', () => {
    setViewportHeigth();
    pageScrollHandler();
    setLayoutShift();
    toggleMenu();
    initSwipers();
    initAnimations();
})

import 'swiper/css'
import './scss/app.scss'
